//============================================================================
// <Description>     :<Main file for wcstools java wrapper>
// <File>            :<main.cpp>
// <Type>            :<c code>
// <Author>          :<Rafael Morales rmorales@iaa.es>
// <Creation date>   :<04 August 2017>
// <History>         :<None>
//============================================================================

//============================================================================
//Include section
//============================================================================

//============================================================================
//System include

//============================================================================
//User include
#include "com_common_geometry_utils_Cgal__.hpp"
#include "ellipse.hpp"
//============================================================================
//System include

#include <iostream>
using namespace std;

//============================================================================
//Declaration section
//============================================================================


//============================================================================
//External functions
//============================================================================

//============================================================================
//Code section
//============================================================================
JNIEXPORT void JNICALL Java_com_common_geometry_utils_Cgal_00024_ellipseFit
  (JNIEnv * env
 , jobject obj
 , jdoubleArray point_seq
 , jlong point_count
 , jdoubleArray fit) {

    double * local_point_seq = (double *) env->GetDoubleArrayElements(point_seq, 0);
    if (local_point_seq == NULL) {
       printf("\n Error in Java_com_common_geometry_utils_Cgal_00024_ellipseFit: local_point_seq");
       return;
    }

    double * local_fit = (double *) env->GetDoubleArrayElements(fit, 0);
    if (local_fit == NULL) {
       printf("\n Error in Java_com_common_geometry_utils_Cgal_00024_ellipseFit: local_fit");
       return;
    }

    //catch not degenerated cases
    try { ellipse_fit(local_point_seq, point_count, local_fit);}
    catch(const std::exception &){}


    fflush(stdout);
    env->ReleaseDoubleArrayElements(point_seq, (jdouble *)local_point_seq, 0);
    env->ReleaseDoubleArrayElements(fit, (jdouble *)local_fit, 0);
}
//============================================================================
//============================================================================

//============================================================================
//End of file:main.c
//============================================================================
