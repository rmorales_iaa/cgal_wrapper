//Java Wrapper for cgal
//https://github.com/CGAL/cgal
//-----------------------------------------------------------------------------

#include <iostream>
using namespace std;

#include "ellipse.hpp"
//-----------------------------------------------------------------------------
void fit_ellipse(void) {

  int point_count = 5;
  double fit[5];

  double point_seq[] = {341.0, 1840.0
		              , 342.0, 1840.0
					  , 343.0, 1840.0
					  , 344.0, 1840.0
					  , 341.0, 1840.0};

  try
  {
    ellipse_fit(point_seq, point_count, fit);
  }
  catch(const std::exception &)
  {
         std::cout << "Invalid input!\n";
  }


  std::cout << "a            = " << fit[0] << std::endl;
  std::cout << "b            = " << fit[1] << std::endl;
  std::cout << "cx           = " << fit[2] << std::endl;
  std::cout << "cy           = " << fit[3] << std::endl;
  std::cout << "theta_degree = " << fit[4] << std::endl;

  /*
   a            = 0.707107
   b            = 0.707107
   cx           = 0.5
   cy           = 0.5
   theta_degree = 180

  */
}
//-----------------------------------------------------------------------------
int main(int argc, char *argv[]) {

	fit_ellipse();
}
//-----------------------------------------------------------------------------
//end of file "main.c"
//-----------------------------------------------------------------------------
