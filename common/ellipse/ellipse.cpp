/*
 * ellipse2D.cpp
 *
 *  Created on: 8 Dec 2021
 *      Author: rafa
 */

//-----------------------------------------------------------------------------
#include "cgal_typedef.hpp"
#include "util.hpp"
//-----------------------------------------------------------------------------
void ellipse_fit(double * point_seq, long point_count, double * fit) {

  Point2D * P =  new Point2D[point_count];
  for(int i=0, k=0; i < point_count; ++i, k+=2)
    P[i] = Point2D(point_seq[k] , point_seq[k+1]);

  Min_ellipse  me2( P, P+point_count, true);

  // get the coefficients
  double A,C,B,D,E,F;
  me2.ellipse().double_coefficients(A, C, B, D, E, F);

  if (me2.is_degenerate()) {
    fit[0] = -1;
	fit[1] = -1;
    fit[2] = -1;
    fit[3] = -1;
    fit[4] = -1;
  }
  else {
	//https://en.wikipedia.org/wiki/Ellipse
	//https://scicomp.stackexchange.com/questions/40494/how-do-i-find-the-minimum-area-ellipse-that-encloses-a-set-of-points

	//semi_axis_major
	fit[0] = -std::sqrt(2*(A*E*E+C*D*D-B*D*E+(B*B-4*A*C)*F)*((A+C)+std::sqrt((A-C)*(A-C)+B*B)))/(B*B-4*A*C);

	//semi_axis_minor
	fit[1]= -std::sqrt(2*(A*E*E+C*D*D-B*D*E+(B*B-4*A*C)*F)*((A+C)-std::sqrt((A-C)*(A-C)+B*B)))/(B*B-4*A*C);

	//centre_x
	fit[2] = (2*C*D-B*E)/(B*B-4*A*C);

	//centre_y
	fit[3] = (2*A*E-B*D)/(B*B-4*A*C);

	double theta = 0;
	if(B!=0) theta = std::atan(1/B*(C-A-std::sqrt((A-C)*(A-C)+B*B)));
	else
	  if(A < C) theta = 0;
      else theta = M_PI; //A > C

	fit[4] = to_degrees(theta);
  }

  delete[] P;
}
//=============================================================================
//End of 'ellipse.cpp'
//=============================================================================
