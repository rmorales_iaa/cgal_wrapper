//=============================================================================
#ifndef CGAL_TYPEDEF_HPP_
#define CGAL_TYPEDEF_HPP_

  //---------------------------------------------------------------------------
  #include <CGAL/Cartesian.h>
  #include <CGAL/Min_ellipse_2.h>
  #include <CGAL/Min_ellipse_2_traits_2.h>
  #include <CGAL/Exact_rational.h>
  #include <cassert>
  //---------------------------------------------------------------------------
  typedef  CGAL::Exact_rational                    NT;
  typedef  CGAL::Cartesian<NT>                     K;
  typedef  CGAL::Point_2<K>                        Point2D;
  typedef  CGAL::Min_ellipse_2_traits_2<K>         Traits;
  typedef  CGAL::Min_ellipse_2<Traits>             Min_ellipse;
  //---------------------------------------------------------------------------

#endif /* CGAL_TYPEDEF_HPP_ */

//=============================================================================
//End of 'cgal_typdef.hpp'
//=============================================================================
